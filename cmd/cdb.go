/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"gitlab.com/zendrulat123/c/dbcon"
	"gitlab.com/zendrulat123/c/condb"
	"github.com/spf13/cobra"
	"log"
)
type User struct{
	ID int
	Name string
	Age int
}
// cdbCmd represents the cdb command
var cdbCmd = &cobra.Command{
	Use:   "cdb",
	Short: "A brief description of your command",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("cdb called")
		db, err := dbcon.GetCon()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(db)
		u:= User{ID: 3, Name: "jen", Age:33}
		fmt.Println(u)
		condb.Dber(u)
	},
}

func init() {
	rootCmd.AddCommand(cdbCmd)

}
